import './home.css';
// Navbar
import AppNavbar from '../components/AppNavbar.js';
import { Container, Row, Col } from 'react-bootstrap';
import Banner from '../components/Banner';
import AboutUs from '../components/AboutUsCard';

export default function Home(){

	return( 

			<>
				<AppNavbar />
				<Container fluid className="homeContainer">
					<Row className="landing">
						<Col className="col-6">
							<Banner />
						</Col>
						<Col className="col-6">
							<img className="landing-img" src="images/landing.png" alt="guitar" />
						</Col>
					</Row>
				</Container>
				<AboutUs />
			</>

		)
}
