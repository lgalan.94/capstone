import { useState, useEffect } from 'react';
import { Container, Row, Col } from 'react-bootstrap';
import ProductCard from '../components/frontComponents/ProductCard';
import AppNavbar from '../components/AppNavbar';

const API = `${process.env.REACT_APP_API_URL}/products/active-products`;

export default function ProductsCatalog() {
  const [products, setProducts] = useState([]);

  useEffect(() => {
    fetch(API)
      .then(result => result.json())
      .then(data => {
        setProducts(
          data.map(product => {
            return <ProductCard key={product._id} productProp={product} />;
          })
        );
      });
  }, []); 

  const renderProductRows = () => {
    const rows = [];
    for (let i = 0; i < products.length; i += 3) {
      rows.push(
        <Row key={i} className="my-3">
          <Col>{products[i]}</Col>
          {i + 1 < products.length && <Col>{products[i + 1]}</Col>}
          {i + 2 < products.length && <Col>{products[i + 2]}</Col>}
        </Row>
      );
    }
    return rows;
  }; 

  return (
    <>
      <AppNavbar />
      <Container>
        <h1 className="mt-3 text-center">Products</h1>
        {renderProductRows()}
      </Container>
    </>
  );
}