import { useState, useContext } from 'react';
import { Modal, Container, Row, Col, Card, Button } from 'react-bootstrap';
import { Link, useNavigate } from 'react-router-dom';
import UserContext from '../../UserContext.js';

const API = `${process.env.REACT_APP_API_URL}/cart/add-to-cart`;

export default function ProductCard(props) {
  const navigate = useNavigate();
  const {user} = useContext(UserContext);

  const { _id, name, description, category, price } = props.productProp;

  const handleAddToCart = () => {
    // make a POST request to the API with the product ID and quantity
    fetch(API, {
      method: 'POST',
      body: JSON.stringify({ id: _id }),
      headers: {
        'Content-Type': 'application/json'
      }
    })
    .then(response => response.json())
    .then(data => {
      console.log(data)
    })
    .catch(error => console.error(error));
  };

  const details = () => {
    if (user.id !== null) {
      navigate(`/products/${_id}`);
    } else {
      navigate('/login')
    }
  }

  return (
    <Container>
      <Row>
        <Col>
          <Card className="productCard">
            <Card.Body>
              <Card.Title className="mb-3 productName">{name}</Card.Title>
              <Card.Subtitle className="cardDescription">Description:</Card.Subtitle>
              <Card.Text>{description}</Card.Text>
              <Card.Subtitle className="cardPrice">Category: </Card.Subtitle>
              <Card.Text>{category}</Card.Text>
              <Card.Subtitle className="cardPrice">Price:</Card.Subtitle>
              <Card.Text>PhP {price}</Card.Text>

              <div className="text-center">
               
                
                  <Button variant="secondary" onClick={() => details()}>
                    Check Details
                  </Button>
              </div>
            </Card.Body>
          </Card>
        </Col>
      </Row>
      </Container>
      )
      
}