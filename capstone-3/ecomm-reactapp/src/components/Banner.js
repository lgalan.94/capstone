import {Button} from 'react-bootstrap';
import{useNavigate} from 'react-router-dom';

export default function Banner(){

const navigate = useNavigate();

	return(
				<>
				
					<h1 className="banner">The guitar store that's always tuned in.</h1>
					<Button className="shopNowBtn mt-3" onClick={() => navigate('/products')} variant="light"><strong>Shop Now</strong></Button>

				
				</>
		  )
} 