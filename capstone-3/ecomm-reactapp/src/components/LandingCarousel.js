import {Container, Row, Col, Carousel} from 'react-bootstrap';
import ProductList from './frontComponents/ProductList';

export default function LandingCarousel(){
	return(
			<>
				<Container className="landingCarouselContainer mt-4">
					<Row>
						<Col className="">
							<Carousel className="productCarousel" data-bs-theme="dark">
						      <Carousel.Item>
						        <img
						          className="d-block w-100"
						          src="./images/c1.png"
						          alt="First slide"
						        />
						        <Carousel.Caption>
						          {/*<h5>First slide label</h5>
						          <p>Nulla vitae elit libero, a pharetra augue mollis interdum.</p>*/}
						        </Carousel.Caption>
						      </Carousel.Item>
						      <Carousel.Item>
						        <img
						          className="d-block w-100"
						          src="./images/c2.png"
						          alt="Second slide"
						        />
						        <Carousel.Caption>
						          {/*<h5>Second slide label</h5>
						          <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>*/}
						        </Carousel.Caption>
						      </Carousel.Item>
						      <Carousel.Item>
						        <img
						          className="d-block w-100"
						          src="./images/c3.png"
						          alt="Third slide"
						        />
						        <Carousel.Caption>
						          {/*<h5>Third slide label</h5>
						          <p>
						            Praesent commodo cursus magna, vel scelerisque nisl consectetur.
						          </p>*/}
						        </Carousel.Caption>
						      </Carousel.Item>
							</Carousel>
						</Col>
						
					</Row>
				</Container>
			</>
		  )
}