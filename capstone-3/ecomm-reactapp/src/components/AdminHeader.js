import React from 'react'
import { Button, Container, Navbar, Nav } from 'react-bootstrap';
import { AiOutlineLogout } from 'react-icons/ai';
import { NavLink } from 'react-router-dom';

const AdminHeader = () => {
    return(
        <> 

            <Navbar position="fixed" className="bg-body-tertiary">
              <Container fluid>
                <Navbar.Brand fixed="top" as={NavLink} to='/admin/dashboard' ><small><strong> ADMIN PANEL </strong></small></Navbar.Brand>
                <Navbar.Toggle />
                <Navbar.Collapse className="justify-content-end">
                  <Nav.Link as = {NavLink} to = '/logout' ><AiOutlineLogout size={24} />Logout</Nav.Link>
                </Navbar.Collapse>
              </Container>
            </Navbar>   
        
      </>
    )
}


  

export default AdminHeader