import React, { useState } from 'react';
import { Table, Container, Button, Modal } from 'react-bootstrap';
import moment from 'moment';

const OrdersData = ({ orders }) => {
  const [showModal, setShowModal] = useState(false);
  const [selectedOrder, setSelectedOrder] = useState(null);

  // Group orders by order ID and user ID
  const groupedOrders = orders.reduce((acc, order) => {
    const orderId = order.orderId;
    const userId = order.userId;
    if (!acc[orderId]) { 
      acc[orderId] = {};
    }
    if (!acc[orderId][userId]) {
      acc[orderId][userId] = [];
    }
    acc[orderId][userId].push(order);
    return acc;
  }, {});

  // Generate table rows for each order ID and user ID
  let i = 0;
  const tableRows = Object.keys(groupedOrders).map((orderId) => {
    const users = groupedOrders[orderId];
    return Object.keys(users).map((userId) => {
      i++;
      const userOrders = users[userId];
      const totalAmount = userOrders.reduce((acc, order) => acc + order.totalAmount, 0);
      const products = userOrders.flatMap((order) => order.products);
      const email = userOrders[0].email;
      const status = userOrders[0].status;
      return (
        <tr className="table-data" key={`${orderId}-${userId}`}>
          <td>{orderId}</td>
          <td>{email}</td>
          <td>{products.map((product) => product.productName).join(', ')}</td>
          <td>{totalAmount}</td>
          <td>{status}</td>
          <td className="text-center">
            <Button
              className="btn-sm"
              variant="dark"
              onClick={() => {
                setSelectedOrder(products);
                setShowModal(true);
              }}
            >
              View
            </Button>
          </td>
        </tr>
      );
    });
  }).flat();

  return (
    <>
      <Table striped bordered hover>
        <thead className="table-primary">
          <tr>
            <th>Order ID</th>
            <th>Email</th>
            <th>Products</th>
            <th>Total Amount</th>
            <th>Status</th>
            <th>Action</th>
          </tr>
        </thead>
        <tbody key="table-body">{tableRows}</tbody>
      </Table>
      {showModal && (
        <Modal show={showModal} onHide={() => setShowModal(false)}>
          <Modal.Header closeButton>
            <Modal.Title>Order Details</Modal.Title>
          </Modal.Header>
          <Modal.Body>
            <Table striped bordered hover>
              <thead className="table-primary">
                <tr>
                  <th>Product Name</th>
                  <th>Quantity</th>
                  <th>Price</th>
                  <th>SubTotal</th>
                </tr>
              </thead>
              <tbody key="table-body">
                {selectedOrder.map((product) => (
                  <tr key={product.productId}>
                    <td>{product.productName}</td>
                    <td>{product.quantity}</td>
                    <td>{product.price}</td>
                    <td>{product.quantity * product.price}</td>
                  </tr>
                ))}
              </tbody>
            </Table>
          </Modal.Body>
          <Modal.Footer>
            <Button variant="secondary" onClick={() => setShowModal(false)}>
              Close
            </Button>
          </Modal.Footer>
        </Modal>
      )}
    </>
  );
};

export default OrdersData;