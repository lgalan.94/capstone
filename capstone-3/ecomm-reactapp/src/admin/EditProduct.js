import React, { useContext, useState, useEffect } from 'react';
import { useParams, useNavigate } from 'react-router-dom';
import { Form, Button } from 'react-bootstrap';
import AdminHeader from '../components/AdminHeader.js';
import AdminNavbar from '../components/AdminNavbar.js';
import UserContext from '../UserContext.js';
import Swal2 from 'sweetalert2';

const EditProduct = () => {
  const { productId } = useParams();
  const [name, setName] = useState('');
  const [description, setDescription] = useState('');
  const [price, setPrice] = useState('');
  const { setUser, User } = useContext(UserContext);

  const navigate = useNavigate();

  useEffect(() => {
    fetch(`${process.env.REACT_APP_API_URL}/products/${productId}`)
      .then(result => result.json())
      .then(data => {
        setName(data.name);
        setDescription(data.description);
        setPrice(data.price);
      })
      .catch(error => console.error(error));
  }, [productId]);

  const handleSubmit = (event) => {
    event.preventDefault();

    fetch(`${process.env.REACT_APP_API_URL}/products/${productId}`, {
      method: 'PATCH',
      headers: {
        'Content-Type': 'application/json',
        'Authorization': `Bearer ${localStorage.getItem('token')}`
      },
      body: JSON.stringify({
        name,
        description,
        price
      })
    })
    .then(response => response.json())
    .then(data => {
      console.log(data);

        if (data === true) {
            Swal2.fire({
              title: "Product Details Successfull Updated!",
              icon: "success"
            }) 

            navigate('/admin/products')

        }  else {
              alert("Error updating product details!")
            }
      // Redirect to the product details page after editing the product
    })
    .catch(error => console.error(error));
  };

  return (
    <>
      <AdminHeader />
      <AdminNavbar />
      <div className="container mt-5">
        <h1>Edit Product</h1>
        <Form onSubmit={handleSubmit}>
          <Form.Group controlId="name">
            <Form.Label>Name</Form.Label>
            <Form.Control type="text" value={name} onChange={(event) => setName(event.target.value)} />
          </Form.Group>

          <Form.Group controlId="description">
            <Form.Label>Description</Form.Label>
            <Form.Control as="textarea" rows={3} value={description} onChange={(event) => setDescription(event.target.value)} />
          </Form.Group>

          <Form.Group controlId="price">
            <Form.Label>Price</Form.Label>
            <Form.Control type="number" value={price} onChange={(event) => setPrice(event.target.value)} />
          </Form.Group>

          <div className="mt-5">
            <Button variant="primary" type="submit">
              Save Changes
            </Button>
          </div>
        </Form>
      </div>
    </>
  );
};

export default EditProduct;