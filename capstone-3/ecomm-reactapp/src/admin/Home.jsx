import AdminHeader from '../components/AdminHeader.js';
import AdminNavbar from '../components/AdminNavbar.js';
import Dashboard from './Dashboard.js';

export default function AdminHome() {
	return (

				<>
					<AdminHeader />
					<AdminNavbar />
					<Dashboard />
				</>

		)
} 