import AdminHeader from '../components/AdminHeader.js';
import AdminNavbar from '../components/AdminNavbar.js';
import OrdersTable from '../components/OrdersTable.js';

export default function Orders() {
	return(

			<>
				<AdminHeader />
				<AdminNavbar />
				<OrdersTable />
			</>
		)
} 