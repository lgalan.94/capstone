import AdminHeader from '../components/AdminHeader.js';
import AdminNavbar from '../components/AdminNavbar.js';
import UsersTable from '../components/UsersTable.js';

export default function Users() {
	return(

			<>
				<AdminHeader />
				<AdminNavbar />
				<UsersTable />
			</>
		)
} 