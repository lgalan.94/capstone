import AppNavbar from '../components/AppNavbar.js';
import {Container, Row, Col, Button, Form} from 'react-bootstrap';
import {useState, useEffect} from 'react';
import {Link, useNavigate} from 'react-router-dom';
import Swal2 from 'sweetalert2';
import Footer from '../components/frontComponents/Footer';


export default function Register(){

	const [userName,setUsername] = useState('');
	const [email, setEmail] = useState('');
	const [password, setPassword] = useState('');
	const [password2, setPassword2] = useState('');
	const [isDisabled, setIsDisabled] = useState(true);

	const navigate = useNavigate();

	useEffect(() => {

		if (userName !== '' && email !== '' && password !== '' && password2 !== '' && password === password2) {
			setIsDisabled(false);
		} else {
			setIsDisabled(true)
		}

	}, [userName, email, password, password2]) //dependencies

	function Register(event) {
		event.preventDefault();

		fetch(`${process.env.REACT_APP_API_URL}/user/register-user`, {
				method: 'POST',
				headers: {
					'Content-type': 'application/json'
				},
				withCredentials: true,
				body: JSON.stringify({
					userName: userName,
					email: email,
					password: password
				})		
		})
		.then(result => result.json())
		.then(data => {
			if (data === false) {
				Swal2.fire({
					title: 'Error',
					icon: 'error',
					text: 'Email is already in used!'
				})
			} else {

				Swal2.fire({
					title: 'Success',
					icon: 'success',
					text: 'Registration Successful'
				})

				navigate('/login');
			}
		})

		/*localStorage.setItem('email', email);
		setUser(localStorage.getItem('email'));

		alert('Thank you for registering!');

		navigate('/');*/

		//clear input fields
		// setEmail('');
		// setPassword1('');
		// setPassword2('');
} 

	return(
			<>
			<Container fluid className="registerPage">
			<AppNavbar />
			
			<Container className = "mt-5 registerContainer">
				<Row className="regRow">
					<Col>
					</Col>
					<Col className = "col-6 mx-auto">
					   <div className="formRegister float-end mb-4">
						<h1 className = "text-center">Register</h1>
						<Form onSubmit = {event => Register(event)}>

								  <Form.Group className="mb-3" controlId="userName">
								    <Form.Control value = {userName} onChange = {event => setUsername(event.target.value)} type="text" placeholder="Username" />
								  </Form.Group>

							      <Form.Group className="mb-3" controlId="email">
							        <Form.Control value = {email} onChange = {event => setEmail(event.target.value)} type="email"  placeholder="Email" />
							        
							      </Form.Group>

							      <Form.Group className="mb-3" controlId="formBasicPassword">
							        <Form.Control value = {password} onChange = {event => setPassword(event.target.value)} type="password" placeholder="Password" />
							      </Form.Group>

							      <Form.Group className="mb-3" controlId="formConfirmPassword">
							        <Form.Control value = {password2} onChange = {event => setPassword2(event.target.value)} type="password" placeholder="Confirm Password" />
							      </Form.Group>

							      <p>Have An Account Already? <Link as = {Link} to = '/login'>Login Here</Link></p>
							      <div className="text-center">
							      <Button className="regBtn" onClick={() =>Register} disabled = {isDisabled} variant="dark" type="submit">
							        Submit
							      </Button>
							      </div>
						    </Form>
						 </div>
					</Col>
				</Row>
			</Container>
			</Container>
			<Footer />
			</>
		)
}