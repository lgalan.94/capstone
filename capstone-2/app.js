const express = require('express');
const cors = require('cors');
const mongoose = require('mongoose');
const userRoutes = require('./routers/userRoutes.js');
const orderRoutes = require('./routers/orderRoutes.js');
const productRoutes = require('./routers/productRoutes.js');
const shoppingCartRoutes = require('./routers/shoppingCartRoutes.js');


const port = 3005;
const app = express();

mongoose.connect("mongodb+srv://admin:admin@batch288galan.q03dxqw.mongodb.net/E-Commerce-API?retryWrites=true&w=majority", { useNewUrlParser : true }, { useUnifiedToplogy : true });

const db = mongoose.connection;
db.on("error", console.error.bind(console, "Error database connection!"));
db.once("open", () => console.log("Successfully connected to cloud database!"));


// middlewares
app.use(express.json());
app.use(express.urlencoded({ extended : true }));

app.use(cors());
app.use('/user', userRoutes);
app.use('/products', productRoutes);
app.use('/orders', orderRoutes);
app.use('/cart', shoppingCartRoutes);


app.listen(port, () => console.log(`Server is running on port ${port}`));