const Product = require('../models/Products.js');
const auth = require('../auth.js');

/*module.exports.addProduct = (request, response) => {

	const userData = auth.decode(request.headers.authorization);

	if(userData.isAdmin){
		let newProduct = new Product({
			name : request.body.name,
			description : request.body.description,
			price : request.body.price,
			createdBy : userData.id
		})

		newProduct.save()
		.then(saved => response.send(true))
		.catch(err => false)
	} else {
		return response.send(false)
	}

}*/



module.exports.addProducts = (request, response) => {
  const userData = auth.decode(request.headers.authorization);

  if (userData.isAdmin) {
    const products = request.body.map(product => new Product ({
      name: product.name,
      description: product.description,
      category: product.category,
      price: product.price,
      createdBy: userData.id
    }));

    Product.insertMany(products)
      .then(saved => response.send(true))
      .catch(err => response.send(false));
  } else {
    response.send(false);
  }
};

/*Retrieve All Products*/

module.exports.getAllProducts = (request, response) => {

	const userData = auth.decode(request.headers.authorization);

	if (userData.isAdmin) {
		Product.find({})
		.then(result => response.send(result))
		.catch(err => response.send(err))
	} else {
		return response.send(false);
	}
}

/*Retrieve Active Products*/

module.exports.getActiveProducts = (request, response) => {

	Product.find({ isActive : true })
	.then(result => response.send(result))
	.catch(err => response.send(false))
}


module.exports.countActiveProduct = (request, response) => {
  Product.find({ isActive: true })
    .then(result => {
      response.status(200).json(result.length);
    })
    .catch(err => {
      console.error(err);
      response.status(500).json({ error: "Failed to count active products!" });
    });
};


/*Retrieve single product (Retrieved only if product is active)*/

module.exports.getProduct = (request, response) => {
	const productId = request.params.productId; 

	Product.findById(productId)
	.then(result => {
		if (result.isActive == true) {
			return response.send(result)
		} else {
			return response.send(false) //Product is Inactive, cannot retrieve product information!
		}
	} )
	.catch(err => response.send(false))

}

/*Update A Product (Admin only)*/

module.exports.updateProduct = (request, response) => {


	console.log("update product is called")
	const userData = auth.decode(request.headers.authorization);

	let updatedProduct = {
		name : request.body.name,
		description : request.body.description,
		price : request.body.price
	}

	if(userData.isAdmin) {
		Product.findByIdAndUpdate(request.params.productId, updatedProduct)
		.then(result => response.send(true))
		.catch(err => response.send(false))
	} else {
		return response.send(false)
	}
}

/*Activate or Archive a Product*/

module.exports.archiveOrActivateProduct = (request, response) => {
	
	const userData = auth.decode(request.headers.authorization);

	const productId = request.params.productId;
	const archivedProduct = {isActive: request.body.isActive}

	if(userData.isAdmin && request.body.isActive == true) {
		Product.findByIdAndUpdate(productId, archivedProduct)
		.then(result => response.send(true)) //Product activated!
		.catch(err => response.send(false))

	}  else if (userData.isAdmin && request.body.isActive == false){
		Product.findByIdAndUpdate(productId, archivedProduct)
		.then(result => response.send(true)) //Product archived
		.catch(err => response.send(false))
		
	} else {
		return response.send(false)
	}
}