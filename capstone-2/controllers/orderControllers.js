const Order = require('../models/Orders.js');
const Product = require('../models/Products.js');
const User = require('../models/Users.js');
const auth = require('../auth.js');
const mongoose = require('mongoose');
const Types = mongoose.Types;


/*Create multiple orders at a time (Inputs: productId and quantity only in Postman)*/

module.exports.checkOut = (request, response) => {

	console.log("checkOut is called")

	const userData = auth.decode(request.headers.authorization);
	
	const newOrders = request.body.orders;

	const orderId = new mongoose.Types.ObjectId();

	if (!userData.isAdmin) {
		const orders = newOrders.map(async order => {
			const product = await Product.findById(order.productId); // find product by ID
			const totalAmount = order.quantity * product.price; // calculate total amount
			return new Order({
				orderId: orderId,
				userId: userData.id,
				email: userData.email,
				products: [{
					productId: product._id,
					quantity: order.quantity,
					price: product.price,
					productName: product.name,
				}],
				totalAmount: totalAmount // set total amount
			})
		})

		Promise.all(orders) // wait for all orders to be created
			.then(createdOrders => {
				return Order.insertMany(createdOrders) // insert all orders at once
			})
			.then(saved => response.send(true))
			.catch(err => response.send(false)) 
		
	} else {
		return response.send(false)
	}
}


module.exports.addItemToCart = (request, response) => {

	const productId = request.body.id;
	const userData = auth.decode(request.headers.authorization);

	if(userData.isAdmin){
		return response.send(false)
	} else {
	 let isUserUpdated = User.findOne({ _id : userData.id })
		.then(result => {
			result.shoppingCart.push({productId : productId})
			
			result.save()
			.then(saved => true)
			.catch(error => false)

		}) 
		.catch(error => false)

		if(isUserUpdated){
			return response.send(true)
		} else {
			return response.send(false)
		}

	}

}


/*Retrieve authenticated user’s orders*/

module.exports.getOrders = (request, response) => {

	const userData = auth.decode(request.headers.authorization);

	Order.find({ userId : userData.id })
	.then(result => {
		if(result.length === 0 ) {
			return response.send(false)
		} else {
			return response.send(result)
		}
	})
	.catch(err => response.send(false))

}


/*Retrieve All Orders*/

module.exports.getAllOrders = (request, response) => {

	const userData = auth.decode(request.headers.authorization);

	if (userData.isAdmin) {
		Order.find({})
		.then(orders => {
			if (orders.length == 0) {
				return response.send(false);
			} else {
				return response.send(orders)
			}
		})
		.catch(error => response.send(false))
	} else {
		return response.send(false);
	}

}