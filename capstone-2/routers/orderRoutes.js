const express = require('express');
const orderControllers = require('../controllers/orderControllers.js');
const auth = require('../auth.js');

const router = express.Router();

/*Create Order*/
router.post('/checkout', auth.verify, orderControllers.checkOut);
router.post('/add-cart', auth.verify, orderControllers.addItemToCart);

/*Get Orders By User*/
router.get('/my-orders', orderControllers.getOrders);

/*Get All Orders (Admin Only)*/
router.get('/all-orders', auth.verify, orderControllers.getAllOrders);

module.exports = router;