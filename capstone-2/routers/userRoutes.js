const express = require('express');
const userControllers = require('../controllers/userControllers');
const auth = require('../auth.js');

const router = express.Router();

//no params routes
router.post('/register-user', userControllers.registerUser);
router.post('/login', userControllers.userLogin);
router.get('/profile', auth.verify, userControllers.getProfile);
router.get("/user-details", auth.verify, userControllers.retrieveUserDetails);
router.get('/count-users', userControllers.countUsers)
router.get('/users-list', auth.verify, userControllers.getUsersList)

//with params routes
router.patch('/:userId/set-user-as-admin', auth.verify, userControllers.setUser);

module.exports = router; 