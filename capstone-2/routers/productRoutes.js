const express = require('express');
const productControllers = require('../controllers/productControllers.js');
const auth = require('../auth.js')

const router = express.Router();

/*Adding products (Admin Only)*/
router.post('/add-product', auth.verify, productControllers.addProducts);

/*Retrieving all products (Admin Only)*/
router.get('/all-products', auth.verify, productControllers.getAllProducts);

/*Getting active products (any-user)*/
router.get('/active-products', productControllers.getActiveProducts);
/*Count active products*/
router.get('/products-count', productControllers.countActiveProduct);

/*WITH PARAMS*/

//Updating a product (Admin only)
router.patch('/:productId', auth.verify, productControllers.updateProduct);

//Getting a product by id (Retrieve only if active)
router.get('/:productId', productControllers.getProduct);

//Archiving and activating a product
router.patch('/:productId/archive', auth.verify, productControllers.archiveOrActivateProduct);

module.exports = router;