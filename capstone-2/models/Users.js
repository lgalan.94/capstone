const mongoose = require('mongoose');

const userSchema = new mongoose.Schema({

	userName : {
		type : String,
		required : [true, "Username is required"]
	},

	email : {
		type : String,
		required: [true, "Email is required!"]
	},

	password : {
		type : String,
		required : [true, "Password is required!"]
	}, 

	isAdmin : {
		type : Boolean,
		default : false
	},

	createdOn : {
		type : Date,
		default : new Date()
	},


	shoppingCart : [

			{
				userName: {
					type: String,
					required: true
				},

				userId: {
					type: String,
					required: true
				},
				
				products: [

						{
							productId: {
								type: String,
								required: true
							},

							productName: {
								type: String,
								required: true
							},

							quantity: {
								type: Number,
								required: true
							},

							subTotal: {
								type: Number,
								required: true
							},

							dateAdded: {
								type: Date,
								default: new Date()
							}
						}

					],

				totalAmount: {
					type: Number,
					required: true
			}


	}
]})

const User = mongoose.model('User', userSchema);

module.exports = User;